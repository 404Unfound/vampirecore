Vampire
=======
Vampire is a minecraft version of the board game Werewolf. The setup includes VampireCore, VampireGame, VampireQueue and VampireRewards.

Contributing
------------

We happily accept contributions, especially through merge requests on GitLab.
Submissions must be licensed under the GNU Lesser General Public License v3.

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for important guidelines to follow.

Links
-----
* [Discord](https://discord.gg/Jp7zqNZ)
* [Twitter](https://twitter.com/SimonChambs00)
