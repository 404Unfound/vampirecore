package me.unfound.vampirecore.Events;

import lombok.Getter;
import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerVoteEvent extends Event {

    private static final HandlerList HANDLERS = new HandlerList();

    @Getter
    private Player whoVoted;

    @Getter
    private Player votedPlayer;

    public PlayerVoteEvent(Player user, Player victim) {
        Validate.notNull(user);
        this.whoVoted = user;
        this.votedPlayer = victim;
    }

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

}
