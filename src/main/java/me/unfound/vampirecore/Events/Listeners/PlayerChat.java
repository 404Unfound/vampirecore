package me.unfound.vampirecore.Events.Listeners;

import me.unfound.vampirecore.Objects.Rank;
import me.unfound.vampirecore.VampireCore;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChat implements Listener {

    private VampireCore core;

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {

        FileConfiguration rank = core.getRanksFile().toFileConfiguration();
        FileConfiguration config = core.getConfig().toFileConfiguration();
        FileConfiguration players = core.getPlayers().toFileConfiguration();
        Player p = e.getPlayer();

        String playerRank = core.getUm().getUser(p).getRank();

        String color = ChatColor.translateAlternateColorCodes('&', rank.getString("ranks." + playerRank + ".Settings.chatColor"));
        String msg = color + e.getMessage();
        String nameColor = ChatColor.translateAlternateColorCodes('&', rank.getString("ranks." + playerRank + ".Settings.nameColor"));
        String name = nameColor + p.getDisplayName();
        String prefix = rank.getString("ranks." + playerRank + ".Settings.prefix").replace('&', '§');
        String suffix = rank.getString("ranks." + playerRank + ".Settings.suffix").replace('&', '§');
        String separator = rank.getString("ranks." + playerRank + ".Settings.chatSeparator").replace('&', '§');

        String format = prefix + name + suffix + separator + msg;

        e.setFormat(format);
    }

}
