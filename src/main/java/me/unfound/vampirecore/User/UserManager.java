package me.unfound.vampirecore.User;

import me.unfound.vampirecore.Objects.User;
import me.unfound.vampirecore.VampireCore;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class UserManager {

    private VampireCore core;
    private HashMap<UUID, User> users = new HashMap<>();

    public UserManager(VampireCore core) {
        this.core = core;
    }

    public User getUser(Player p) {
        if (users.isEmpty()) {
            fetchUser(p);
        }
        if (users.containsKey(p.getUniqueId())) {
            return users.get(p.getUniqueId());
        } else {
            fetchUser(p);
            if (users.containsKey(p.getUniqueId())) {
                return users.get(p.getUniqueId());
            }
        }
        return null;
    }

    public String getOfflineName(String uuid) {
        return null;
    }

    public String getOfflineUUID(String name) {
        return null;
    }

    private void fetchUser(Player p) {

    }

    public void saveUsers() {

    }

    public void saveUser(boolean single, User user) {

    }

    public void createNewUser(Player p) {
        createNewUser(p.getUniqueId().toString(), p.getName());
    }

    public void createNewUser(String uuid, String name) {

    }

    public void deleteUser(String uuid) {

    }


}