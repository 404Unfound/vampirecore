package me.unfound.vampirecore.Objects;

import lombok.Getter;
import lombok.Setter;
import me.unfound.vampirecore.VampireCore;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {

    @Getter
    @Setter
    private String rank;

    @Getter
    @Setter
    private UUID uuid;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private List<String> perms;

    @Getter
    private PermissionAttachment attachment;

    @Getter
    private boolean hasLoadedPerms = false;

    @Getter
    @Setter
    private String permString;

    public User(UUID p) {

        this.name = Bukkit.getPlayer(p).getName();

    }

    public void loadPerms(String s, Player player) {
        perms = new ArrayList<>();
        permString = s;
        if (s != null) {
            String[] parts = s.split(":");
            for (String part : parts) {
                if (part != null) {
                    perms.add(part);
                }
            }
        }
        if (player != null) {
            attachment = player.addAttachment(VampireCore.getCore());
            if (!perms.isEmpty()) {
                for (String str : perms) {
                    attachment.setPermission(str, true);
                }
            }
            if (VampireCore.getCore().getIdToRank().containsKey(getRank())) {

            } else {
                System.out.println("User loaded from database has a rank that is not loaded in-game. Their UUID is: " + getUuid());
            }
            hasLoadedPerms = true;
        }

    }
}
