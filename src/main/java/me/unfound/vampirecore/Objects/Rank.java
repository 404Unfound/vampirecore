package me.unfound.vampirecore.Objects;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.ChatColor;

import java.util.ArrayList;

public class Rank {

    @Setter
    @Getter
    boolean isDefault;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private int level;
    @Getter
    @Setter
    private ArrayList<String> permissions;
    @Getter
    @Setter
    private ArrayList<String> inherit;

    public Rank(String id, String prefix, ChatColor color, ArrayList<String> perms, ArrayList<String> inherit, boolean isDefault) {
        this.name = name;
        this.level = level;
        this.permissions = perms;
        this.isDefault = isDefault;
        this.inherit = inherit;
    }


}
