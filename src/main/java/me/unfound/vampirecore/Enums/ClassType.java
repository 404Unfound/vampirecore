package me.unfound.vampirecore.Enums;

import java.util.Arrays;
import java.util.Optional;

public enum ClassType {

    Vampire(1),
    Seer(2),
    Villager(3),
    Witch(4),
    AlphaWerewolf(5),
    Doctor(6),
    Insomniac(7),
    Angel(8),
    Hunter(9),
    Assassin(10);


    private final int id;

    private ClassType(final int id) {
        this.id = id;
    }


    public Optional<ClassType> getTypeById(int id) {
        return Arrays.stream(values())
                .filter(ClassType -> ClassType.id == id)
                .findFirst();
    }


}