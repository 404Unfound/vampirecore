package me.unfound.vampirecore;

import lombok.Getter;
import lombok.Setter;
import me.unfound.vampirecore.DataStorage.DataFile;
import me.unfound.vampirecore.Objects.Rank;
import me.unfound.vampirecore.User.UserManager;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class VampireCore extends JavaPlugin {

    private static VampireCore vampireCore;

    @Getter
    private DataFile config;

    @Getter
    private DataFile messages;

    @Getter
    private DataFile players;

    @Getter
    private DataFile ranksFile;

    @Getter
    private DataFile classFile;

    @Getter
    private UserManager um;

    @Getter
    private Map<String, Rank> idToRank = new HashMap<>();

    @Getter
    @Setter
    private List<String> RanksList;

    @Getter
    private Rank defaultRank;

    public static VampireCore getCore() {
        return vampireCore;
    }

    @Override
    public void onEnable() {
        vampireCore = this;
        config = new DataFile(this, "config", true);
        messages = new DataFile(this, "messages", true);
        players = new DataFile(this, "players", false);
        ranksFile = new DataFile(this, "ranks", false);
        classFile = new DataFile(this, "class", false);
    }

    @Override
    public void onDisable() {
        vampireCore = null;

    }

    public void loadRanks() {
        FileConfiguration rankConfig = YamlConfiguration.loadConfiguration(new File(""));
        ConfigurationSection rankSection = rankConfig.getConfigurationSection("ranks");
        for (String s : rankSection.getKeys(false)) {
            Rank rank;
        }
    }

}
